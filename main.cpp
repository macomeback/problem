//
//  main.cpp
//  Problem
//

#include <iostream>
#include <math.h>
#include "IntegralCalculator.h"

using namespace std;

int main() {
    IntegralCalculator integralCalculator;
    int n;
    double from,to;
    std::cout<<"Enter polynomial degree and start and end points.\n";
    std::cin>>n>>from>>to;
    double p[n+1];
    std::cout<<"Enter coefficients from highest to lowest.\n";
    for(int i=n;i>=0;i--)
        std::cin>>p[i];
    std::cout<<integralCalculator.exactIntegralCalculate(p, n, from, to)<<"\n";
    std::cout<<integralCalculator.numericalIntegralCalculate(p, n, from, to)<<"\n";
    return 0;
}
