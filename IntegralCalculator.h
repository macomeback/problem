//
//  IntegralCalculator.h
//  Problem


#ifndef IntegralCalculator_h
#define IntegralCalculator_h
class IntegralCalculator{
private:
    double polynomialCalculate(double* coefficients,int degree,double at){
        double result=0;
        double atPower=1;
        for(int i=0;i<=degree;i++){
            result=result+atPower*coefficients[i];
            atPower*=at;
        }
        return result;
    }
    double floorIntegralCalulate(double* coefficients,int degree,double from,double to,int chunksNumber){
        double result=0;
        double chunkSize= (to-from)/chunksNumber;
        for(int i=0;i<chunksNumber;i++){
            double x= from + ((to-from)*i)/chunksNumber;
            result= result+polynomialCalculate(coefficients, degree, x);
        }
        return result*chunkSize;
    }
public:
    double exactIntegralCalculate(double* coefficients,int degree,double from,double to){
        double result=0;
        double fromPower=from,toPower=to;
        for(int i=0;i<=degree;i++){
            result=result+(coefficients[i]*(toPower-fromPower))/(i+1);
            fromPower*=from;
            toPower*=to;
        }
        return result;
    }
    //0.01 precision
    double numericalIntegralCalculate(double* coefficients,int degree,double from,double to){
        double floor,ceiling,chunkSize;
        int chunksNumber=100;
        do{
            chunkSize= (to-from)/chunksNumber;
            floor=floorIntegralCalulate(coefficients, degree, from, to, chunksNumber);
            ceiling= floor+(polynomialCalculate(coefficients, degree, to)-polynomialCalculate(coefficients, degree, from))*chunkSize;
            chunksNumber*=2;
        }while (abs(floor-ceiling)>0.02);
        return (floor+ceiling)/2;
    }
};

#endif /* IntegralCalculator_h */
